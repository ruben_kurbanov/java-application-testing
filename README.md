**Semestrální práce z předmětu B6B36TS1**

Obsah semestrální práce:

- Složka DBS-Semestral, která obsahuje jednoduchou desktopovou aplikaci související s provozem databáze. Také testy pro tuto aplikaci.

- .gitignor - ignoruje nepotřebné soubory a složky

- Dokumentace semestrální práce

- README.md

