package service;

import dao.DaoJepartnerem;
import model.Farmar;
import model.Jepartnerem;
import model.Trh;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class JepartneremService {

    DaoJepartnerem daoJ;

    public JepartneremService(DaoJepartnerem daoJ){
        this.daoJ = daoJ;
    }

    public boolean checkString(String string) {
        try {
            Integer.parseInt(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void save(Farmar farmar, Trh trh, String[] s, LocalDate[] localDates) throws Exception{

        //jestli existujou zaznamy farmar i trh
        if (trh == null){
            showErrorMsage("With this ID trh, the record does not exist in the database!");
            throw new Exception();
        }
        if (farmar == null){
            showErrorMsage("With this ID farmar, the record does not exist in the database!");
            throw new Exception();
        }

        //jestli id trh nebo farmar je int
        if(!checkString(s[1]) || !checkString(s[0])){
            showErrorMsage("The entered id value is incorrect!");
            throw new Exception();
        }

        //jestli farmar aktualne pracuje s license
        if (!farmar.isActive()){
            showErrorMsage("This farmar does not have a valid license or has ceased operations!");
            throw new Exception();
        }

        //jestli from neni vetsi nez to
        if(localDates[0].isAfter(localDates[1])){
            showErrorMsage("The interval is set incorrectly!");
            throw new Exception();
        }

        //jestli perioda neni vetsi 8 let a neni mensi 1 roku
        Period period = Period.between(localDates[0], localDates[1]);
        if(period.getYears() < 1 || period.getYears() > 8){
            showErrorMsage("The contract conclusion period is possible from 1 to 8 years!");
            throw new Exception();
        }

        //jestli se license konci drive, nez se konci contract
        Date date = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        if (farmar.getLicense().before(date)){

            showErrorMsage("This farmer's license expires before the contract!");
            throw new Exception();
        }

        //jestli date from je validni
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate startDate1 = LocalDate.parse("01.03.2017", formatter);
        LocalDate endDate1 = LocalDate.parse("01.05.2029", formatter);
        if (localDates[0].isBefore(startDate1) || localDates[0].isAfter(endDate1)){

            showErrorMsage("The start date of the contract was entered incorrectly!");
            throw new Exception();
        }

        //jestli date to je validni
        LocalDate startDate2 = LocalDate.parse("01.03.2018", formatter);
        LocalDate endDate2 = LocalDate.parse("01.05.2029", formatter);
        if (localDates[1].isBefore(startDate2) || localDates[1].isAfter(endDate2)){

            showErrorMsage("The date of the end of the contract was entered incorrectly!");
            throw new Exception();
        }

        //jestli trh a farmar ze stejnich mest
        if(!trh.getMesto().equals(farmar.getMesto())){
            showErrorMsage("Organizations are located in different cities!");
            throw new Exception();
        }

        //jestli stejny zaznam s jiz neexistuje
        List<Jepartnerem> list = daoJ.findAll();

        for (Jepartnerem l : list){
            if (l.getId_trh() == trh.getId_trh() && l.getId_farmar() == farmar.getId_farmar()){
                showErrorMsage("Such an entry already exists in the database!");
                throw new Exception();
            }
        }

        Date date1 = Date.from(localDates[0].atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date date2 = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        Jepartnerem p = new Jepartnerem(trh.getId_trh(), farmar.getId_farmar(), date1, date2);
        daoJ.save(p);

    }

    public void update(Farmar farmar, Trh trh, String[] s, LocalDate[] localDates, Jepartnerem oldRecord) throws Exception {

        //jestli existujou zaznamy farmar i trh
        if (trh == null){
            showErrorMsage("With this ID trh, the record does not exist in the database!");
            throw new Exception();
        }
        if (farmar == null){
            showErrorMsage("With this ID farmar, the record does not exist in the database!");
            throw new Exception();
        }

        //jestli id trh nebo farmar je int
        if(!checkString(s[1]) || !checkString(s[0])){
            showErrorMsage("The entered id value is incorrect!");
            throw new Exception();
        }

        //jestli farmar aktualne pracuje s license
        if (!farmar.isActive()){
            showErrorMsage("This farmar does not have a valid license or has ceased operations!");
            throw new Exception();
        }

        //jestli from neni vetsi nez to
        if(localDates[0].isAfter(localDates[1])){
            showErrorMsage("The interval is set incorrectly!");
            throw new Exception();
        }

        //jestli perioda neni vetsi 8 let a neni mensi 1 roku
        Period period = Period.between(localDates[0], localDates[1]);
        if(period.getYears() < 1 || period.getYears() > 8){
            showErrorMsage("The contract conclusion period is possible from 1 to 8 years!");
            throw new Exception();
        }

        //jestli se license konci drive, nez se konci contract
        Date date = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        if (farmar.getLicense().before(date)){

            showErrorMsage("This farmer's license expires before the contract!");
            throw new Exception();
        }

        //jestli date from je validni
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate startDate1 = LocalDate.parse("01.03.2017", formatter);
        LocalDate endDate1 = LocalDate.parse("01.05.2029", formatter);
        if (localDates[0].isBefore(startDate1) || localDates[0].isAfter(endDate1)){

            showErrorMsage("The start date of the contract was entered incorrectly!");
            throw new Exception();
        }

        //jestli date to je validni
        LocalDate startDate2 = LocalDate.parse("01.03.2018", formatter);
        LocalDate endDate2 = LocalDate.parse("01.05.2029", formatter);
        if (localDates[1].isBefore(startDate2) || localDates[1].isAfter(endDate2)){

            showErrorMsage("The date of the end of the contract was entered incorrectly!");
            throw new Exception();
        }

        //jestli trh a farmar ze stejnich mest
        if(!trh.getMesto().equals(farmar.getMesto())){
            showErrorMsage("Organizations are located in different cities!");
            throw new Exception();
        }

        Date date1 = Date.from(localDates[0].atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date date2 = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        Jepartnerem pNew = new Jepartnerem(Integer.parseInt(s[0]),Integer.parseInt(s[1]), date1, date2);

        daoJ.delete(oldRecord);
        daoJ.save(pNew);

    }

    public void delete(int id_trh, int id_farmar, Date contract_from, Date contract_to){

        Jepartnerem j = new Jepartnerem(id_trh, id_farmar, contract_from, contract_to);
        daoJ.delete(j);

    }

    public Jepartnerem find(String id_trh, String id_farmar) throws Exception {
        List<Jepartnerem> list = daoJ.findAll();

        if (!checkString(id_trh) && !checkString(id_farmar)){
            throw new Exception("Invalid ID!");
        }else{
            for (Jepartnerem p: list){
                if (p.getId_trh() == Integer.parseInt(id_trh)
                        && p.getId_farmar() == Integer.parseInt(id_farmar)){
                    return p;
                }
            }

        }

        return null;
    }

    public List<Jepartnerem> findAll(){
        return daoJ.findAll();
    }

    public void showErrorMsage(String s){
        System.err.print(s);
    }

}
