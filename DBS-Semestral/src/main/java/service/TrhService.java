package service;

import dao.DaoJepartnerem;
import dao.DaoTrh;
import model.Jepartnerem;
import model.Trh;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TrhService {

    DaoTrh daoTrh;
    DaoJepartnerem daoJepa;

    public TrhService(DaoTrh daoTrh, DaoJepartnerem daoJepa) {
        this.daoTrh = daoTrh;

        this.daoJepa = daoJepa;

    }

    public List<Trh> findAll() {
        return daoTrh.findAll();
    }

    public Trh find(String s) throws Exception {

        if (checkString(s)) {
            return daoTrh.findById(Integer.parseInt(s));
        } else {
            showErrorMsage("Invalid ID!");
            throw new Exception();
        }
    }

    public void save(String[] s) throws Exception {

        if (!checkString(s[0])){
            showErrorMsage("Invalid ID!");
            throw new Exception();
        }

        if(daoTrh.findById(Integer.parseInt(s[0])) != null){
            showErrorMsage("A record with the same ID already exists!");
            throw new Exception();
        }

        if (s[2].equals("") || s[2].length() > 20) {
            showErrorMsage("Name is empty or too long!");
            throw new Exception();
        }

        if (s[4].length() > 12 || !isDigit(s[4])){
            showErrorMsage("Wrong phone number!");
            throw new Exception();
        }

        if (s[1].length() > 64 || !isWord(s[1])){
            showErrorMsage("City is too long or wrong!");
            throw new Exception();
        }

        daoTrh.save(new Trh(Integer.parseInt(s[0]), s[1], s[2], Integer.parseInt(s[3]), s[4]));
    }

    public void update(String[] s) throws Exception {

        if (!checkString(s[0])){
            showErrorMsage("Invalid ID!");
            throw new Exception();
        }

        if(daoTrh.findById(Integer.parseInt(s[0])) == null){
            showErrorMsage("A record with this ID no exists!");
            throw new Exception();
        }

        if (s[2].equals("") || s[2].length() > 20) {
            showErrorMsage("Name is empty or too long!");
            throw new Exception();
        }

        if (s[4].length() > 12 || !isDigit(s[4])){
            showErrorMsage("Wrong phone number!");
            throw new Exception();
        }

        if (s[1].length() > 64 || !isWord(s[1])){
            showErrorMsage("City is too long or wrong!");
            throw new Exception();
        }

        daoTrh.update(new Trh(Integer.parseInt(s[0]), s[1], s[2], Integer.parseInt(s[3]), s[4]));
    }

    public void delete(Trh trh) throws Exception {
        List<Jepartnerem> partners = daoJepa.findAll();
        for (Jepartnerem p: partners){
            if(p.getId_trh() == trh.getId_trh()){
                showErrorMsage("It is not possible to delete this object, because it is related to Farmar in another table!");
                throw new Exception();
            }
        }

        daoTrh.delete(trh);

    }

    public boolean checkString(String string) {
        try {
            Integer.parseInt(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isWord(String s) {
        Pattern pattern = Pattern.compile("[a-zA-Z]{1,}");
        Matcher matcher = pattern.matcher(s);

        return matcher.matches();
    }

    public boolean isDigit(String s) {
        Pattern pattern = Pattern.compile("[0-9]{1,}");
        Matcher matcher = pattern.matcher(s);

        return matcher.matches();
    }

    public void showErrorMsage(String s){
        System.err.print(s);
    }
}