package service;

import dao.DaoFarm;
import dao.DaoJepartnerem;
import model.Farmar;
import model.Jepartnerem;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FarmService {

    DaoFarm daoFarm;
    DaoJepartnerem daoJepa;

    public FarmService(DaoFarm daoFarm){
        this.daoFarm = daoFarm;
    }

    public FarmService(DaoFarm daoFarm, DaoJepartnerem daoJepa){
        this.daoFarm = daoFarm;
        this.daoJepa = daoJepa;
    }

    public Farmar find(String s) throws Exception{

        if (!checkString(s)){
            showErrorMsage("Invalid ID!");
            throw new Exception();
        }else{
            int id = Integer.parseInt(s);
            return daoFarm.findById(id);
        }

    }

    public Farmar save(String[] s, LocalDate date, boolean active) throws Exception{

        if (s == null){
            showErrorMsage("Expect NullPointerException!");
            throw new Exception();
        }

        //jestli id je int
        if (!checkString(s[0])){
            showErrorMsage("Invalid id data!");
            throw new Exception();
        }

        //jestli name je vice nez 20 pismen
        if (s[3].length() > 20 ){
            showErrorMsage("Invalid name data!");
            throw new Exception();
        }

        //jestli email je validni a neni vetsi 64 simbolu
        if(s[1].length() > 64 || !isEmail(s[1])){

            showErrorMsage("Invalid email format! For example kurbarub@fel.cvut.cz");
            throw new Exception();
        }

        //jestli telofon je validni, neni prazdny, a neni vesi nez 12
        if (s[4].length() > 12 ||
                s[4].isEmpty() || !isDigit(s[4])){

            showErrorMsage("Invalid Phone data!");
            throw new Exception();
        }

        // jestli mesto je validni
        if (s[2].isEmpty() || s[2].length() > 64
                || !isWord(s[2])){

            showErrorMsage("Invalid city data!");
            throw new Exception();
        }

        //jestli license je validni
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate startDate2 = LocalDate.parse("01.01.1900", formatter);
        if (date == null){
            showErrorMsage("The date of the license was entered incorrectly!");

            throw new Exception();
        }
        Date date1 = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

        if (date.isBefore(startDate2) || date1.getYear() > new Date().getYear() + 10){
            showErrorMsage("The date of the license was entered incorrectly!");
            throw new Exception();
        }

        Date d = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

        Farmar f = new Farmar(Integer.parseInt(s[0]), s[1], s[2], s[3],
                s[0], d, active);

        if (daoFarm.findById(Integer.parseInt(s[0])) == null){
            daoFarm.save(f);
            return f;
        }else{
            showErrorMsage("A record with the same ID already exists!");
            throw new Exception();
        }

    }

    public Farmar update(String[] s, LocalDate date, boolean active) throws Exception {

        if (s == null){
            showErrorMsage("Expect NullPointerException!");
            throw new Exception();
        }

        //jestli id je int
        if (!checkString(s[0])){
            showErrorMsage("Invalid id data!");
            throw new Exception();
        }

        //jestli name je vice nez 20 pismen
        if (s[3].length() > 20 ){
            showErrorMsage("Invalid name data!");
            throw new Exception();
        }

        //jestli email je validni a neni vetsi 64 simbolu
        if(s[1].length() > 64 || !isEmail(s[1])){

            showErrorMsage("Invalid email format! For example kurbarub@fel.cvut.cz");
            throw new Exception();
        }

        //jestli telofon je validni, neni prazdny, a neni vesi nez 12
        if (s[4].length() > 12 ||
                s[4].isEmpty() || !isDigit(s[4])){

            showErrorMsage("Invalid Phone data!");
            throw new Exception();
        }

        // jestli mesto je validni
        if (s[2].isEmpty() || s[2].length() > 64
                || !isWord(s[2])){

            showErrorMsage("Invalid city data!");
            throw new Exception();
        }

        //jestli license je validni
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate startDate2 = LocalDate.parse("01.01.1900", formatter);
        if (date == null){
            showErrorMsage("The date of the license was entered incorrectly!");

            throw new Exception();
        }
        Date date1 = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

        if (date.isBefore(startDate2) || date1.getYear() > new Date().getYear() + 10){
            showErrorMsage("The date of the license was entered incorrectly!");
            throw new Exception();
        }

        Date d = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

        Farmar f = new Farmar(Integer.parseInt(s[0]), s[1], s[2], s[3],
                s[0], d, active);

        if (daoFarm.findById(Integer.parseInt(s[0])).getId_farmar() ==
                Integer.parseInt(s[0])){
            daoFarm.update(f);
            return f;
        }else{
            showErrorMsage("Invalid data, check and try again!");
            throw new Exception();
        }
    }

    public Farmar delete(Farmar farmar) throws Exception {

        List<Jepartnerem> partners = daoJepa.findAll();
        for (Jepartnerem p: partners){
            if(p.getId_farmar() == farmar.getId_farmar()){
                showErrorMsage("It is not possible to delete this object, because it is related to Trh in another table!");
                throw new Exception();
            }
        }
        daoFarm.delete(farmar);
        return farmar;
    }

    public List<Farmar> findAll(){return daoFarm.findAll();}


    public boolean isEmail(String s){
        Pattern pattern = Pattern.compile("(\\w+[\\.-]{0,1}\\w+)+@(\\w+[\\.-]{0,1}\\w+)+[\\.]{1}[a-z]{2,4}");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

    public boolean isWord(String s) {
        Pattern pattern = Pattern.compile("[a-zA-Z]{1,}");
        Matcher matcher = pattern.matcher(s);

        return matcher.matches();
    }

    public boolean isDigit(String s) {
        Pattern pattern = Pattern.compile("[0-9]{1,}");
        Matcher matcher = pattern.matcher(s);

        return matcher.matches();
    }

    public boolean checkString(String string) {
        try {
            Integer.parseInt(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void showErrorMsage(String s){
        System.err.print(s);
    }
}
