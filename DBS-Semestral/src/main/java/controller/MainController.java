package controller;

import dao.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import model.Farmar;
import model.Jepartnerem;
import model.Trh;
import service.FarmService;
import service.JepartneremService;
import service.TrhService;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainController {

    DaoFarm daoF = new DaoFarm();
    DaoTrh daoT = new DaoTrh();
    DaoJepartnerem daoJ = new DaoJepartnerem();

    FarmService serviceF = new FarmService(daoF, daoJ);
    JepartneremService serviceJ = new JepartneremService(daoJ);
    TrhService serviceT = new TrhService(daoT, daoJ);

    //**
    // Relationship Window Variables and methods------------------------------------------------
    // */

    //Left panel
    @FXML
    private Pane pane_vertical_relation;

    @FXML
    private Label label_relation;
    @FXML
    private TextField id_trh;

    @FXML
    private TextField id_farmar;

    @FXML
    private DatePicker contract_from_relat;

    @FXML
    private DatePicker contract_to_relat;

    @FXML
    private Button add_new_relat;

    @FXML
    private Button update_ok_relat;

    @FXML
    private Button update_cancel_relat;

    @FXML
    private Button view_trhy;

    @FXML
    private Button view_farmar;

    //Bottom panel
    @FXML
    private Pane pane_horizont_relation;

    @FXML
    private Button delete_relation;

    @FXML
    private Button load_data_relation_table;

    @FXML
    private Label lable_loading_data_relation_table;

    //Top panel
    @FXML
    private ScrollPane scroll_pane_relation;

    //Table relation
    @FXML
    private TableView<Jepartnerem> relation_table;

    @FXML
    private TableColumn<Jepartnerem, String> trh_column;

    @FXML
    private TableColumn<Jepartnerem, String> farmar_column;

    @FXML
    private TableColumn<Jepartnerem, Date> column_contr_from;

    @FXML
    private TableColumn<Jepartnerem, Date> column_contr_to;

    //Methods

    //Ukáže stránku s data pro farmáři
    @FXML
    private void viewFarmar(ActionEvent event){
        cleanArchoPane();

        pane_vertical_farmar.setVisible(true);
        pane_horizont_farmar.setVisible(true);
        scroll_pane_farmar.setVisible(true);
    }

    //Ukáže stránku s data pro trhy
    @FXML
    private void viewTrhy(ActionEvent event){
        cleanArchoPane();

        pane_vertical_trh.setVisible(true);
        pane_horizont_trh.setVisible(true);
        scroll_pane_trh.setVisible(true);
    }

    //Tlačitko, po stisknutí vytvoří nový zaznám relation v database
    @FXML
    public void newRelation(ActionEvent event) throws Exception {

        String[] s = new String[]{
                id_farmar.getText(),
                id_trh.getText()
        };

        LocalDate[] localDates = new LocalDate[]{
                contract_from_relat.getValue(),
                contract_to_relat.getValue()
        };

        serviceJ.save(serviceF.find(s[0]), serviceT.find(s[1]), s, localDates);

        id_trh.setText("");
        id_farmar.setText("");
        contract_from_relat.setValue(null);
        contract_to_relat.setValue(null);
        loadingDataRelation();


    }


    //proměnna, která uchovává vybraný v tabulce záznam relationship
    private Jepartnerem selectionData = null;

    //odstraní vybraný v tabulce záznam z databaze
    @FXML
    public void deleteRelation(ActionEvent event) {

        if(selectionData != null){
            serviceJ.delete(selectionData.getId_trh(), selectionData.getId_farmar(), selectionData.getContract_from(), selectionData.getContract_to());
            loadingDataRelation();
            selectionData = null;
        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    // zavolá metodu pro uložení dat z database do tabulky
    @FXML
    public void loadDataRelationTable(ActionEvent event) {
        //System.out.println(contract_from_relat.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));

        loadingDataRelation();
    }

    //ukaže formulař pro aktualizace vybraného záznamu
    @FXML
    public void updateRelation(ActionEvent event) {

        if (selectionData != null){
            id_trh.setText(Integer.toString(selectionData.getId_trh()));
            id_farmar.setText(Integer.toString(selectionData.getId_farmar()));

            contract_from_relat.setValue(selectionData.getContract_from().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            contract_to_relat.setValue(selectionData.getContract_to().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

            label_relation.setText("Update relationship");
            add_new_relat.setVisible(false);
            view_farmar.setVisible(false);
            view_trhy.setVisible(false);
            pane_horizont_relation.setVisible(false);
            update_ok_relat.setVisible(true);
            update_cancel_relat.setVisible(true);
        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    //tlačitko, která oznamuje systém, že uživatel podtverdí, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updateRelationPushOK(ActionEvent event) throws Exception {

        String[] s = new String[]{
                id_trh.getText(),
                id_farmar.getText()
        };
        LocalDate[] localDates = new LocalDate[]{
                contract_from_relat.getValue(),
                contract_to_relat.getValue()
        };

        Jepartnerem oldRecord = new Jepartnerem(selectionData.getId_trh(),selectionData.getId_farmar(), selectionData.getContract_from(), selectionData.getContract_to());

        serviceJ.update(serviceF.find(s[1]), serviceT.find(s[0]), s, localDates, oldRecord);
        id_trh.setText("");
        id_farmar.setText("");
        contract_from_relat.setValue(null);
        contract_to_relat.setValue(null);
        loadingDataRelation();
        selectionData = null;

        label_relation.setText("New relationship");
        add_new_relat.setVisible(true);
        view_farmar.setVisible(true);
        view_trhy.setVisible(true);
        pane_horizont_relation.setVisible(true);
        update_ok_relat.setVisible(false);
        update_cancel_relat.setVisible(false);

    }

    //tlačitko, která oznamuje systém, že uživatel odmitne, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updateRelationPushCancel(ActionEvent event) {

        id_trh.setText("");
        id_farmar.setText("");
        contract_from_relat.setValue(null);
        contract_to_relat.setValue(null);
        loadingDataRelation();
        selectionData = null;

        label_relation.setText("New relationship");
        add_new_relat.setVisible(true);
        view_farmar.setVisible(true);
        view_trhy.setVisible(true);
        pane_horizont_relation.setVisible(true);
        update_ok_relat.setVisible(false);
        update_cancel_relat.setVisible(false);
    }

    //metoda pro uložení dat z database do tabulky
    public void loadingDataRelation(){

        TableView.TableViewSelectionModel<Jepartnerem> selectionModel = relation_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<Jepartnerem>(){

            public void changed(ObservableValue<? extends Jepartnerem> observable, Jepartnerem oldValue, Jepartnerem newValue) {
                if(newValue != null) selectionData = newValue;
            }

        });
        ObservableList<Jepartnerem> data = FXCollections.observableArrayList();

        List<Jepartnerem> list = serviceJ.findAll();

        for (Jepartnerem l : list){
            data.add(new Jepartnerem(l.getId_trh(), l.getId_farmar(), l.getContract_from(), l.getContract_to()));
        }

        this.trh_column.setCellValueFactory(new PropertyValueFactory<Jepartnerem, String>("id_trh"));
        this.farmar_column.setCellValueFactory(new PropertyValueFactory<Jepartnerem, String>("id_farmar"));
        this.column_contr_from.setCellValueFactory(new PropertyValueFactory<Jepartnerem, Date>("contract_from"));
        this.column_contr_to.setCellValueFactory(new PropertyValueFactory<Jepartnerem, Date>("contract_to"));
        this.column_contr_from.setCellFactory(column -> {
            TableCell<Jepartnerem, Date> cell = new TableCell<Jepartnerem, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        setText(format.format(item));
                    }
                }
            };

            return cell;
        });

        this.column_contr_to.setCellFactory(column -> {
            TableCell<Jepartnerem, Date> cell = new TableCell<Jepartnerem, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        setText(format.format(item));
                    }
                }
            };

            return cell;
        });
        this.relation_table.setItems(data);
    }


    //**
    // Farmar Window Variables and methods----------------------------------------------
    // */

    //Left panel
    @FXML
    private Pane pane_vertical_farmar;

    @FXML
    private Label farmar_lable;

    @FXML
    private TextField id_new_farmar;

    @FXML
    private TextField name_new_farmar;

    @FXML
    private TextField email_new_farmar;

    @FXML
    private TextField phone_new_farmar;

    @FXML
    private TextField city_new_farmar;

    @FXML
    private Button add_new_farmar;

    @FXML
    private Button back_farmar;

    @FXML
    private Button ok_update_farmar;

    @FXML
    private Button canc_update_farmar;

    @FXML
    private CheckBox checkbox_active_farmar;

    @FXML
    private DatePicker license_to_farmar;

    //Bottom panel
    @FXML
    private Pane pane_horizont_farmar;

    @FXML
    private Button update_farmar;

    @FXML
    private Button delete_farmar;

    //Top pane
    @FXML
    private ScrollPane scroll_pane_farmar;

    //Table farmar
    @FXML
    private TableView<Farmar> farmar_table;

    @FXML
    private TableColumn<Farmar, Integer> id_farmar_column;

    @FXML
    private TableColumn<Farmar, String> name_farmar_column;

    @FXML
    private TableColumn<Farmar, String> email_farmar_column;

    @FXML
    private TableColumn<Farmar, String> phone_farmar_column;

    @FXML
    private TableColumn<Farmar, String> city_farmar_column;

    @FXML
    private TableColumn<Farmar, Date> column_license;

    @FXML
    private TableColumn<Farmar, Boolean> column_active;

    //Methods

    //Tlačitko, po stisknutí vytvoří nový zaznám farmaři v database
    @FXML
    public void newFarmar(ActionEvent event) throws Exception {

        String[] s = new String[]{
                id_new_farmar.getText(),
                email_new_farmar.getText(),
                city_new_farmar.getText(),
                name_new_farmar.getText(),
                phone_new_farmar.getText()
        };

        serviceF.save(s, license_to_farmar.getValue(), checkbox_active_farmar.isSelected());

        id_new_farmar.setText("");
        email_new_farmar.setText("");
        city_new_farmar.setText("");
        name_new_farmar.setText("");
        phone_new_farmar.setText("");
        license_to_farmar.setValue(null);
        checkbox_active_farmar.setSelected(false);

        loadingDataFarmar();

    }

    //tlačitko pro vracení do main stránky s tabulkou relationships
    @FXML
    public void backFarmPush(ActionEvent event) {
        cleanArchoPane();

        pane_vertical_relation.setVisible(true);
        pane_horizont_relation.setVisible(true);
        scroll_pane_relation.setVisible(true);
    }

    //odstraní záznam vybraného farmaři
    @FXML
    public void deleteFarmar(ActionEvent event) throws Exception {
        if(selectionDataFarmar != null){
            Farmar f = new Farmar(selectionDataFarmar.getId_farmar(), selectionDataFarmar.getEmail(),
                    selectionDataFarmar.getMesto(), selectionDataFarmar.getName(),
                    selectionDataFarmar.getTelefon(), selectionDataFarmar.getLicense(), selectionDataFarmar.isActive());

            serviceF.delete(f);
            loadingDataFarmar();
            selectionDataFarmar = null;
        }else{
            showErrorMsage("Choose one of the options in the table!");

        }
    }

    //ukaže formulař pro aktualizace vybraného záznamu
    @FXML
    public void updateFarmar(ActionEvent event) {

        if(selectionDataFarmar != null){
            id_new_farmar.setText(Integer.toString(selectionDataFarmar.getId_farmar()));
            name_new_farmar.setText(selectionDataFarmar.getName());
            email_new_farmar.setText(selectionDataFarmar.getEmail());
            phone_new_farmar.setText(selectionDataFarmar.getTelefon());
            city_new_farmar.setText(selectionDataFarmar.getMesto());
            license_to_farmar.setValue(selectionDataFarmar.getLicense().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            checkbox_active_farmar.setSelected(selectionDataFarmar.isActive());

            farmar_lable.setText("Update Farmar");
            add_new_farmar.setVisible(false);
            ok_update_farmar.setVisible(true);
            canc_update_farmar.setVisible(true);
            pane_horizont_farmar.setVisible(false);
            back_farmar.setVisible(false);

        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    //tlačitko, která oznamuje systém, že uživatel podtverdí, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushOkFarmar(ActionEvent event) throws Exception {

        String[] s = new String[]{id_new_farmar.getText(),
                email_new_farmar.getText(),
                city_new_farmar.getText(),
                name_new_farmar.getText(),
                phone_new_farmar.getText()
        };

        serviceF.update(s,license_to_farmar.getValue(), checkbox_active_farmar.isSelected());

        id_new_farmar.setText("");
        email_new_farmar.setText("");
        city_new_farmar.setText("");
        name_new_farmar.setText("");
        phone_new_farmar.setText("");
        license_to_farmar.setValue(null);
        checkbox_active_farmar.setSelected(false);
        loadingDataFarmar();
        selectionDataFarmar = null;
        farmar_lable.setText("New Farmar");
        add_new_farmar.setVisible(true);
        ok_update_farmar.setVisible(false);
        canc_update_farmar.setVisible(false);
        pane_horizont_farmar.setVisible(true);
        back_farmar.setVisible(true);

    }

    //tlačitko, která oznamuje systém, že uživatel odmitne, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushCancelFarmar(ActionEvent event) {

        id_new_farmar.setText("");
        email_new_farmar.setText("");
        city_new_farmar.setText("");
        name_new_farmar.setText("");
        phone_new_farmar.setText("");
        license_to_farmar.setValue(null);
        checkbox_active_farmar.setSelected(false);
        selectionDataFarmar = null;
        loadingDataFarmar();

        farmar_lable.setText("New Farmar");
        add_new_farmar.setVisible(true);
        ok_update_farmar.setVisible(false);
        canc_update_farmar.setVisible(false);
        pane_horizont_farmar.setVisible(true);
        back_farmar.setVisible(true);
    }

    //zavolá metodu pro vložení dat z databaze do tabulky
    @FXML
    public void loadDataFarmarTable(ActionEvent event) {

        loadingDataFarmar();
    }

    //proměnna, která uchovává vybraný v tabulce záznam farmar
    private Farmar selectionDataFarmar = null;

    //metoda pro vložení dat z databaze do tabulky
    public void loadingDataFarmar(){
        TableView.TableViewSelectionModel<Farmar> selectionModel = farmar_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<Farmar>(){

            public void changed(ObservableValue<? extends Farmar> observable, Farmar oldValue, Farmar newValue) {
                if (newValue != null) selectionDataFarmar = newValue;
            }

        });
        ObservableList<Farmar> data = FXCollections.observableArrayList();

        List<Farmar> list = serviceF.findAll();
        for (Farmar l : list){
            data.add(new Farmar( l.getId_farmar(), l.getEmail(), l.getMesto(), l.getName(), l.getTelefon(), l.getLicense(),l.isActive()));
        }

        this.id_farmar_column.setCellValueFactory(new PropertyValueFactory<Farmar, Integer>("id_farmar"));
        this.name_farmar_column.setCellValueFactory(new PropertyValueFactory<Farmar, String>("name"));
        this.email_farmar_column.setCellValueFactory(new PropertyValueFactory<Farmar, String>("email"));
        this.phone_farmar_column.setCellValueFactory(new PropertyValueFactory<Farmar, String>("telefon"));
        this.city_farmar_column.setCellValueFactory(new PropertyValueFactory<Farmar, String>("mesto"));
        this.column_license.setCellValueFactory(new PropertyValueFactory<Farmar, Date>("license"));
        this.column_license.setCellFactory(column -> {
            TableCell<Farmar, Date> cell = new TableCell<Farmar, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        setText(format.format(item));
                    }
                }
            };

            return cell;
        });
        this.column_active.setCellValueFactory(new PropertyValueFactory<Farmar, Boolean>("active"));

        this.farmar_table.setItems(data);
    }

    //**
    // Trh Window Variables and methods----------------------------------------------
    // */

    //Left panel
    @FXML
    private Pane pane_vertical_trh;

    @FXML
    private Label trh_lable;

    @FXML
    private TextField id_new_trh;

    @FXML
    private TextField name_new_trh;

    @FXML
    private TextField phone_new_trh;

    @FXML
    private TextField psc_new_trh;

    @FXML
    private TextField city_new_trh;

    @FXML
    private Button add_new_trh;

    @FXML
    private Button back_trh;

    @FXML
    private Button ok_update_trh;

    @FXML
    private Button canc_update_trh;

    //Bottom panel
    @FXML
    private Pane pane_horizont_trh;

    @FXML
    private Button update_trh;

    @FXML
    private Button delete_trh;

    //Top pane
    @FXML
    private ScrollPane scroll_pane_trh;

    //Table farmar
    @FXML
    private TableView<Trh> trh_table;

    @FXML
    private TableColumn<Trh, Integer> id_trh_column;

    @FXML
    private TableColumn<Trh, String> name_trh_column;

    @FXML
    private TableColumn<Trh, String> phone_trh_column;

    @FXML
    private TableColumn<Trh, Integer> psc_trh_column;

    @FXML
    private TableColumn<Trh, String> city_trh_column;

    private Trh selectionDataTrh = null;

    //Methods

    //odstraní vybraný v tabulce záznam z database
    @FXML
    public void deleteTrh(ActionEvent event) throws Exception {
        if(selectionDataTrh != null){
            Trh t = new Trh(selectionDataTrh.getId_trh(), selectionDataTrh.getMesto(), selectionDataTrh.getName(),
                    selectionDataTrh.getPsc(), selectionDataTrh.getTelefon());

            serviceT.delete(t);
            loadingDataTrh();
            selectionDataFarmar = null;
        }else{
            showErrorMsage("Choose one of the options in the table!");
        }
    }


    //ukaže formulař pro aktualizace vybraného záznamu
    @FXML
    public void updateTrh(ActionEvent event) {

        if(selectionDataTrh != null){
            id_new_trh.setText(Integer.toString(selectionDataTrh.getId_trh()));
            name_new_trh.setText(selectionDataTrh.getName());
            phone_new_trh.setText(selectionDataTrh.getTelefon());
            psc_new_trh.setText(Integer.toString(selectionDataTrh.getPsc()));
            city_new_trh.setText(selectionDataTrh.getMesto());

            trh_lable.setText("Update Trh");
            add_new_trh.setVisible(false);
            pane_horizont_trh.setVisible(false);
            ok_update_trh.setVisible(true);
            canc_update_trh.setVisible(true);
            back_trh.setVisible(false);

        }else{
            showErrorMsage("Choose one of the options in the table!");
        }

    }

    //Tlačitko, po stisknutí vytvoří nový zaznám trhu v database
    @FXML
    public void newTrh(ActionEvent event) throws Exception {

        String[] s = {
                id_new_trh.getText(),
                city_new_trh.getText(),
                name_new_trh.getText(),
                psc_new_trh.getText(),
                phone_new_trh.getText()
        };

        serviceT.save(s);

        id_new_trh.setText("");
        city_new_trh.setText("");
        name_new_trh.setText("");
        psc_new_trh.setText("");
        phone_new_trh.setText("");
        loadingDataTrh();


    }

    //tlačitko pro vracení do main stránky s tabulkou relationships
    @FXML
    public void backTrhPush(ActionEvent event) {
        cleanArchoPane();

        pane_vertical_relation.setVisible(true);
        pane_horizont_relation.setVisible(true);
        scroll_pane_relation.setVisible(true);
    }

    //tlačitko, která oznamuje systém, že uživatel podtverdí, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushOkTrh(ActionEvent event) throws Exception {

        String[] s = {
                id_new_trh.getText(),
                city_new_trh.getText(),
                name_new_trh.getText(),
                psc_new_trh.getText(),
                phone_new_trh.getText()
        };
        serviceT.update(s);

        id_new_trh.setText("");
        city_new_trh.setText("");
        name_new_trh.setText("");
        psc_new_trh.setText("");
        phone_new_trh.setText("");
        loadingDataTrh();
        selectionDataTrh = null;
        trh_lable.setText("New Trh");
        add_new_trh.setVisible(true);
        pane_horizont_trh.setVisible(true);
        ok_update_trh.setVisible(false);
        canc_update_trh.setVisible(false);
        back_trh.setVisible(true);

    }

    //tlačitko, která oznamuje systém, že uživatel odmitne, že chce aktualizovat data vybraného záznamu v databaze
    @FXML
    public void updatePushCancelTrh(ActionEvent event) {
        System.out.println("Push cancel update trh");

        id_new_trh.setText("");
        city_new_trh.setText("");
        name_new_trh.setText("");
        psc_new_trh.setText("");
        phone_new_trh.setText("");

        loadingDataTrh();
        selectionDataTrh = null;

        trh_lable.setText("New Trh");
        add_new_trh.setVisible(true);
        pane_horizont_trh.setVisible(true);
        ok_update_trh.setVisible(false);
        canc_update_trh.setVisible(false);
        back_trh.setVisible(true);
    }

    //zavolá metodu pro vložení dat z databaze do tabulky
    @FXML
    public void loadDataTrhTable(ActionEvent event) {
        loadingDataTrh();
    }


    // metoda pro vložení dat z databaze do tabulky
    public void loadingDataTrh(){

        TableView.TableViewSelectionModel<Trh> selectionModel = trh_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<Trh>(){

            public void changed(ObservableValue<? extends Trh> observable, Trh oldValue, Trh newValue) {

                if (newValue != null) selectionDataTrh = newValue;
            }

        });

        ObservableList<Trh> data = FXCollections.observableArrayList();

        List<Trh> list = serviceT.findAll();

        for (Trh l : list){
            data.add(new Trh( l.getId_trh(),l.getMesto(), l.getName(),l.getPsc(), l.getTelefon()));
        }

        this.id_trh_column.setCellValueFactory(new PropertyValueFactory<Trh, Integer>("id_trh"));
        this.name_trh_column.setCellValueFactory(new PropertyValueFactory<Trh, String>("name"));
        this.phone_trh_column.setCellValueFactory(new PropertyValueFactory<Trh, String>("telefon"));
        this.psc_trh_column.setCellValueFactory(new PropertyValueFactory<Trh, Integer>("psc"));
        this.city_trh_column.setCellValueFactory(new PropertyValueFactory<Trh, String>("mesto"));


        this.trh_table.setItems(data);
    }

    //Uklidí všechny elementy z ArdhoPane
    public void cleanArchoPane(){
        pane_vertical_relation.setVisible(false);
        pane_horizont_relation.setVisible(false);
        scroll_pane_relation.setVisible(false);

        pane_vertical_farmar.setVisible(false);
        pane_horizont_farmar.setVisible(false);
        scroll_pane_farmar.setVisible(false);

        pane_vertical_trh.setVisible(false);
        pane_horizont_trh.setVisible(false);
        scroll_pane_trh.setVisible(false);
    }

    //Okno Chyby
    private JTable jt;

    //Metoda, která přijímá zprávu o chybě a ukáže její
    public void showErrorMsage(String s){
        System.out.println(s);
        JOptionPane.showMessageDialog(new JTable(),s,"ERROR",JOptionPane.ERROR_MESSAGE);

    }
}
