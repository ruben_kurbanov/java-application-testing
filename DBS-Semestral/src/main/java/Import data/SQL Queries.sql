DROP TABLE IF EXISTS Farmarz CASCADE;
DROP TABLE IF EXISTS Trh CASCADE;
DROP TABLE IF EXISTS Zbozi CASCADE;
DROP TABLE IF EXISTS Osoba CASCADE;
DROP TABLE IF EXISTS Emails CASCADE;
DROP TABLE IF EXISTS Pracovnik CASCADE;
DROP TABLE IF EXISTS Zakaznik CASCADE;
DROP TABLE IF EXISTS Objednavka CASCADE;
DROP TABLE IF EXISTS Konzultace CASCADE;
DROP TABLE IF EXISTS Pracuje CASCADE;
DROP TABLE IF EXISTS Nachazise CASCADE;
DROP TABLE IF EXISTS Jepartnerem CASCADE;
DROP TABLE IF EXISTS Vyrabi CASCADE;
DROP TABLE IF EXISTS Obsahuje CASCADE;

CREATE TABLE Farmarz(
    id_farmar INTEGER PRIMARY KEY,
    nazev VARCHAR(20) NOT NULL,
	email VARCHAR(64),
    telefon VARCHAR(12) NOT NULL,
	mesto VARCHAR(64) NOT NULL,
	license DATE NOT NULL,
	active BOOLEAN
);

CREATE TABLE Trh(
    id_trh INTEGER PRIMARY KEY,
    nazev VARCHAR(20) NOT NULL,
    telefon CHAR(12) NOT NULL,
	psc INTEGER,
	mesto VARCHAR(64) NOT NULL
);

CREATE TABLE Jepartnerem(
    id_farmar INTEGER NOT NULL,
    id_trh INTEGER NOT NULL,
	contract_from DATE,
	contract_to DATE,
    PRIMARY KEY (id_farmar, id_trh),
    CONSTRAINT jepartnerem_fk_trh
        FOREIGN KEY (id_trh) 
        REFERENCES Trh(id_trh)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT jepartnerem_fk_farmar
        FOREIGN KEY (id_farmar) 
        REFERENCES Farmarz(id_farmar)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  
);

CREATE TABLE Zbozi(
    id_zbozi INTEGER PRIMARY KEY,
    nazev VARCHAR(64)
);
  
CREATE TABLE Osoba(
    id_osoba INTEGER PRIMARY KEY,
    jmeno VARCHAR(128) NOT NULL,
    primeni VARCHAR(128) NOT NULL,
    telefon CHAR(9) UNIQUE
);
  
CREATE TABLE Emails(
    id_osoba INTEGER PRIMARY KEY,
    email VARCHAR(128) UNIQUE,
    CONSTRAINT email_fk_osoba
        FOREIGN KEY (id_osoba)
        REFERENCES Osoba(id_osoba)
        ON UPDATE CASCADE
        ON DELETE CASCADE
      
);
  
CREATE TABLE Pracovnik(
    id_pracovnik INTEGER,
    id_spravce INTEGER,
    rodne_cislo INTEGER NOT NULL,
    mesto VARCHAR(128) NOT NULL,
    ulice VARCHAR(128) NOT NULL,
    cislo_domu INTEGER NOT NULL,
    psc CHAR(5),
    PRIMARY KEY (id_pracovnik),
    CONSTRAINT pracovnik_ch_psc CHECK (psc LIKE '[0-9][0-9][0-9][0-9][0-9]'),
    CONSTRAINT pracovnik_fk_id 
        FOREIGN KEY (id_pracovnik)
        REFERENCES Osoba(id_osoba)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT pracovnik_fk_spravce 
        FOREIGN KEY (id_spravce)
        REFERENCES Pracovnik(id_pracovnik)
        ON UPDATE CASCADE
        ON DELETE CASCADE
      
);
  
CREATE TABLE Zakaznik(
    id_zakaznik INTEGER PRIMARY KEY,
    CONSTRAINT zakaznik_fk_id 
        FOREIGN KEY (id_zakaznik)
        REFERENCES Osoba(id_osoba)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);
  
CREATE TABLE Objednavka(
    id_zakaznik INTEGER NOT NULL UNIQUE,
    id_trh INTEGER NOT NULL UNIQUE,
    cislo_objednavky INTEGER NOT NULL UNIQUE,
    cislo_kosiku INTEGER NOT NULL UNIQUE,
    CONSTRAINT objednavka_fk_zakaznik
        FOREIGN KEY (id_zakaznik)
        REFERENCES Zakaznik(id_zakaznik)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT objednavka_fk_trh
        FOREIGN KEY (id_trh) 
        REFERENCES Trh(id_trh)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  
);
  
CREATE TABLE Konzultace(
    id_zakaznik INTEGER NOT NULL,
    id_zbozi INTEGER NOT NULL,
    id_pracovnik INTEGER NOT NULL,
    PRIMARY KEY (id_zakaznik, id_zbozi, id_pracovnik),
    CONSTRAINT konzult_fk_zakaznik
        FOREIGN KEY (id_zakaznik)
        REFERENCES Zakaznik(id_zakaznik)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT konzult_fk_zbozi
        FOREIGN KEY (id_zbozi) 
        REFERENCES Zbozi(id_zbozi)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT konzult_fk_pracovnik
        FOREIGN KEY (id_pracovnik) 
        REFERENCES Pracovnik(id_pracovnik)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  
);
  
CREATE TABLE Pracuje(
    id_pracovnik INTEGER NOT NULL,
    id_trh INTEGER NOT NULL,
    PRIMARY KEY (id_pracovnik ,id_trh),
    CONSTRAINT pracuje_fk_trh
        FOREIGN KEY (id_trh) 
        REFERENCES Trh(id_trh)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT pracuje_fk_pracovnik
        FOREIGN KEY (id_pracovnik) 
        REFERENCES Pracovnik(id_pracovnik)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  
);
  
CREATE TABLE Nachazise(
    id_zbozi INTEGER NOT NULL,
    id_trh INTEGER NOT NULL,
    pocet INTEGER DEFAULT 0,
    PRIMARY KEY (id_zbozi, id_trh),
    CONSTRAINT nachazise_fk_trh
        FOREIGN KEY (id_trh) 
        REFERENCES Trh(id_trh)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT nachazise_fk_zbozi
        FOREIGN KEY (id_zbozi) 
        REFERENCES Zbozi(id_zbozi)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  
);
  
CREATE TABLE Vyrabi(
    id_farmar INTEGER NOT NULL,
    id_zbozi INTEGER NOT NULL,
    PRIMARY KEY (id_farmar, id_zbozi),
    CONSTRAINT vyrabi_fk_zbozi
        FOREIGN KEY (id_zbozi) 
        REFERENCES Zbozi(id_zbozi)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT vyrabi_fk_farmar
        FOREIGN KEY (id_farmar) 
        REFERENCES Farmarz(id_farmar)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  
);
  
CREATE TABLE Obsahuje(
    cislo_kosiku INTEGER NOT NULL,
    id_zbozi INTEGER NOT NULL,
    PRIMARY KEY (cislo_kosiku, id_zbozi),
    CONSTRAINT obsahuje_fk_kosikf
        FOREIGN KEY (cislo_kosiku) 
        REFERENCES Objednavka(cislo_kosiku)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT obsahuje_fk_zbozi
        FOREIGN KEY (id_zbozi) 
        REFERENCES Zbozi(id_zbozi)
        ON UPDATE CASCADE
        ON DELETE CASCADE
  
);