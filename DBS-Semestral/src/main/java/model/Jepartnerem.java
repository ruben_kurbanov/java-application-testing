package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "jepartnerem")
public class Jepartnerem implements Serializable {

    @Id
    private Integer id_trh;

    @Id
    private Integer id_farmar;

    private Date contract_from;

    private Date contract_to;

    public Jepartnerem(){

    }

    public Jepartnerem(Integer id_trh, Integer id_farmar, Date contract_from, Date contract_to) {
        this.id_trh = id_trh;
        this.id_farmar = id_farmar;
        this.contract_from = contract_from;
        this.contract_to = contract_to;
    }

    public Integer getId_trh() {
        return id_trh;
    }

    public void setId_trh(int id_trh) {
        this.id_trh = id_trh;
    }

    public Integer getId_farmar() {
        return id_farmar;
    }

    public void setId_farmar(Integer id_farmar) {
        this.id_farmar = id_farmar;
    }

    public Date getContract_from() {
        return contract_from;
    }

    public void setContract_from(Date contract_from) {
        this.contract_from = contract_from;
    }

    public Date getContract_to() {
        return contract_to;
    }

    public void setContract_to(Date contract_to) {
        this.contract_to = contract_to;
    }

}
