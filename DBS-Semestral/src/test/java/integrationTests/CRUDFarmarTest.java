package integrationTests;

import dao.*;
import model.Farmar;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.cglib.core.Local;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CRUDFarmarTest {

    private static Farmar existFarmar = null;


    @BeforeClass
    public static void before(){
        DaoFarm daoFarmar = new DaoFarm();
        LocalDate license = LocalDate.of(2031,5,24);
        Date date = Date.from(license.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar farmar = new Farmar(35, "honzaovoce@email.cz", "Liberec", "HonzaOvoce", "420562985642", date, true);

        if(daoFarmar.findById(35) == null){
            daoFarmar.save(farmar);
        }else{
            existFarmar = daoFarmar.findById(35);
            daoFarmar.delete(existFarmar);
            daoFarmar.save(farmar);
        }   
    }
    @AfterClass
    public static void after(){
        DaoFarm daoFarmar = new DaoFarm();
        LocalDate license = LocalDate.of(2031,5,24);
        Date date = Date.from(license.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar farmar = new Farmar(35, "honzaovoce@email.cz", "Liberec", "HonzaOvoce", "420562985642", date, true);
        daoFarmar.delete((farmar));
        if (existFarmar != null) {
            daoFarmar.save(existFarmar);
        }
    }
    @Test
    @Order(1)
    public void save_Farmar_pozitivni_pruhod(){
        //arrange
        DaoFarm daoFarmar = new DaoFarm();
        Farmar actual = null;
        List<Farmar> list = daoFarmar.findAll();
        //act
        Date license = Date.from(LocalDate.of(2031, 5, 24).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar expect = new Farmar(35, "honzaovoce@email.cz", "Liberec", "HonzaOvoce", "420562985642", license, true);
        daoFarmar.save(expect);
        list = daoFarmar.findAll();
        for (Farmar l : list) {
            if(l.getId_farmar() == 35){
                actual = l;
                break;
            }
        }
        //assert
        assertNotNull(actual);
        assertEquals(expect.getId_farmar(), actual.getId_farmar());
        assertEquals(expect.getEmail(), actual.getEmail());
        assertEquals(expect.getMesto(), actual.getMesto());
        assertEquals(expect.getName(), actual.getName());
        assertEquals(expect.getTelefon(), actual.getTelefon());
        assertEquals(expect.getLicense(), actual.getLicense());
        assertEquals(expect.isActive(), actual.isActive());

    }

    @Test
    @Order(2)
    public void update_Farmar_pozitivni_pruhod(){
        //arrange
        DaoFarm daoFarmar = new DaoFarm();
        Farmar exist = null;
        Farmar actual = null;
        //act
        List<Farmar> list = daoFarmar.findAll();
        for (Farmar l : list) {
            if(l.getId_farmar() == 35){
                exist = l;
                break;
            }
        }
        daoFarmar.delete(exist);
        list = daoFarmar.findAll();

        Date license = Date.from(LocalDate.of(2028, 3, 15).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar expect = new Farmar(35, "honzazel@email.cz", "Praha", "HonzaZel", "420562985642", license, true);
        daoFarmar.save(expect);
        list = daoFarmar.findAll();

        for (Farmar l : list) {
            if(l.getId_farmar() == 35){
                actual = l;
                break;
            }
        }
        //assert
        assertNotNull(exist);
        assertNotNull(actual);
        assertEquals(expect.getId_farmar(), actual.getId_farmar());
        assertEquals(expect.getEmail(), actual.getEmail());
        assertEquals(expect.getMesto(), actual.getMesto());
        assertEquals(expect.getName(), actual.getName());
        assertEquals(expect.getTelefon(), actual.getTelefon());
        assertEquals(expect.getLicense(), actual.getLicense());
        assertEquals(expect.isActive(), actual.isActive());

    }

    @Test
    @Order(3)
    public void delete_Farmar_pozitivni_pruhod(){
        //arrange
        DaoFarm daoFarmar = new DaoFarm();
        Farmar exist = null;
        Farmar actual = null;
        //act
        List<Farmar> list = daoFarmar.findAll();
        for (Farmar l : list) {
            if(l.getId_farmar() == 35){
                exist = l;
                break;
            }
        }
        daoFarmar.delete(exist);
        list = daoFarmar.findAll();
        for (Farmar l : list) {
            if(l.getId_farmar() == 35){
                actual = l;
                break;
            }
        }
        //assert
        assertNotNull(exist);
        assertNull(actual);
    }
    @Test
    @Order(4)
    public void findAll_Farmar_pozitivni_pruhod(){
        //arrange
        DaoFarm daoFarmar = new DaoFarm();
        Farmar actual = null;
        //act
        List<Farmar> list = daoFarmar.findAll();
        List<Farmar> list_real = daoFarmar.findAll();
        Date license = Date.from(LocalDate.of(2031, 5, 24).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar expect = new Farmar(38, "honzaovoce@email.cz", "Liberec", "HonzaOvoce", "420562985642", license, true);
        Farmar farmar1 = new Farmar(23, "test@seznam.cz", "Praha", "Testttt", "420659265495", license, false);
        daoFarmar.save(expect);
        daoFarmar.save(farmar1);
        list = daoFarmar.findAll();
        int newListCount = list.size();
        newListCount = newListCount - 2;
        int realCount = list_real.size();

        daoFarmar.delete(expect);
        daoFarmar.delete(farmar1);

        //assert
        assertNotNull(list);
        assertEquals(newListCount, realCount);
    }
}
