package integrationTests;

import dao.*;
import model.Farmar;
import model.Jepartnerem;
import model.Trh;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class CRUDJepartneremTest {

    String[] s = new String[]{
            "50", //id farmar
            "80" //id trh
    };

    LocalDate[] localDatesOrigin = new LocalDate[]{
            LocalDate.of(2018, 3, 1), //contract from
            LocalDate.of(2023, 3, 1) //contract to
    };

    LocalDate[] localDatesUpdate = new LocalDate[]{
            LocalDate.of(2018, 3, 1), //contract from
            LocalDate.of(2023, 4, 24) //contract to UPDATE
    };

    private static Farmar existFarmar = null;
    private static Trh existTrh = null;

    @BeforeClass
    public static void before(){
        DaoFarm daoF = new DaoFarm();
        DaoTrh daoT = new DaoTrh();

        LocalDate license = LocalDate.of(2024,6,1);
        Date date = Date.from(license.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar farmar = new Farmar(50, "farmartest@seznam.cz", "Brno", "FarmarTest", "420720293520", date, true);

        if(daoF.findById(50) == null){
            daoF.save(farmar);
        }else{
            existFarmar = daoF.findById(50);
            daoF.delete(existFarmar);
            daoF.save(farmar);
        }

        Trh trh = new Trh(80, "Brno", "TrhTest", 33333, "420720293410");

        if (daoT.findById(80) == null){
            daoT.save(trh);
        }else{
            existTrh = daoT.findById(80);
            daoT.delete(existTrh);
            daoT.save(trh);
        }
    }

    @AfterClass
    public static void after(){
        DaoFarm daoF = new DaoFarm();
        DaoTrh daoT = new DaoTrh();

        LocalDate license = LocalDate.of(2024,6,1);
        Date date = Date.from(license.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar farmar = new Farmar(50, "farmartest@seznam.cz", "Brno", "FarmarTest", "420720293520", date, true);
        Trh trh = new Trh(80, "Brno", "TrhTest", 33333, "420720293410");

        daoF.delete(farmar);
        daoT.delete(trh);

        if (existFarmar != null) {
            daoF.save(existFarmar);
        }

        if (existTrh != null) {
            daoT.save(existTrh);
        }

    }

    @Order(1)
    @Test
    public void save_Jepartnerem_pozitivni_pruhod() {

        // arrange
        DaoJepartnerem daoJ = new DaoJepartnerem();
        DaoFarm daoF = new DaoFarm();
        DaoTrh daoT = new DaoTrh();

        Farmar farmar = daoF.findById(Integer.parseInt(s[0]));
        Trh trh = daoT.findById(Integer.parseInt(s[1]));

        //from MainController
        boolean isfreerelation = true;

        Jepartnerem actual = null;


        //act

        //jestli stejny zaznam s relationship jiz neexistuje
        List<Jepartnerem> list = daoJ.findAll();

        for (Jepartnerem l : list) {
            if (l.getId_trh() == Integer.parseInt(s[1]) && l.getId_farmar() == Integer.parseInt(s[0])) {
                isfreerelation = false;
                break;
            }
        }

        boolean actualIsCorrectData = controlDataRelation(farmar, trh, s, localDatesOrigin);

        Date dateFrom = Date.from(localDatesOrigin[0].atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date dateTo = Date.from(localDatesOrigin[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        Jepartnerem expect = new Jepartnerem(Integer.parseInt(s[1]), Integer.parseInt(s[0]), dateFrom, dateTo);

        daoJ.save(expect);

        list = daoJ.findAll();

        for (Jepartnerem l : list) {
            if (l.getId_trh() == Integer.parseInt(s[1]) && l.getId_farmar() == Integer.parseInt(s[0])) {
                actual = l;
                break;
            }

        }

        //assert

        assertTrue(isfreerelation);
        assertTrue(actualIsCorrectData);
        assertNotNull(farmar);
        assertNotNull(trh);
        assertNotNull(actual);

        assertEquals(expect.getId_trh(), actual.getId_trh());
        assertEquals(expect.getId_farmar(), actual.getId_farmar());
        assertEquals(expect.getContract_from(), actual.getContract_from());
        assertEquals(expect.getContract_to(), actual.getContract_to());

    }

    @Order(2)
    @Test
    public void update_Jepartnerem_pozitivni_pruhod(){

        // arrange
        DaoJepartnerem daoJ = new DaoJepartnerem();
        DaoFarm daoF = new DaoFarm();
        DaoTrh daoT = new DaoTrh();

        Farmar farmar = daoF.findById(Integer.parseInt(s[0]));
        Trh trh = daoT.findById(Integer.parseInt(s[1]));

        Jepartnerem relatInDB = null;
        Jepartnerem actual = null;
        boolean isfreerelation = true;

        //act

        List<Jepartnerem> list = daoJ.findAll();

        for (Jepartnerem l : list) {
            if (l.getId_trh() == Integer.parseInt(s[1]) && l.getId_farmar() == Integer.parseInt(s[0])) {
                relatInDB = l;
                break;
            }

        }

        daoJ.delete(relatInDB);

        //jestli stejny zaznam s relationship jiz neexistuje
        list = daoJ.findAll();

        for (Jepartnerem l : list) {
            if (l.getId_trh() == Integer.parseInt(s[1]) && l.getId_farmar() == Integer.parseInt(s[0])) {
                isfreerelation = false;
                break;
            }
        }

        boolean actualIsCorrectData = controlDataRelation(farmar, trh, s, localDatesUpdate);

        Date dateFrom = Date.from(localDatesUpdate[0].atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date dateTo = Date.from(localDatesUpdate[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        Jepartnerem expect = new Jepartnerem(Integer.parseInt(s[1]), Integer.parseInt(s[0]), dateFrom, dateTo);

        daoJ.save(expect);

        list = daoJ.findAll();
        for (Jepartnerem l : list) {
            if (l.getId_trh() == Integer.parseInt(s[1]) && l.getId_farmar() == Integer.parseInt(s[0])) {
                actual = l;
                break;
            }

        }

        //assert
        assertNotNull(relatInDB);
        assertTrue(isfreerelation);
        assertTrue(actualIsCorrectData);
        assertNotNull(actual);
        assertEquals(expect.getId_farmar(), actual.getId_farmar());
        assertEquals(expect.getId_trh(), actual.getId_trh());
        assertEquals(expect.getContract_from(), actual.getContract_from());
        assertEquals(expect.getContract_to(), actual.getContract_to());
    }

    @Order(3)
    @Test
    public void delete_Jepartnerem_pozitivni_pruhod(){
        // arrange
        DaoJepartnerem daoJ = new DaoJepartnerem();

        Jepartnerem relatInDB = null;
        Jepartnerem actual = null;

        //act

        List<Jepartnerem> list = daoJ.findAll();

        for (Jepartnerem l : list) {
            if (l.getId_trh() == Integer.parseInt(s[1]) && l.getId_farmar() == Integer.parseInt(s[0])) {
                relatInDB = l;
                break;
            }
        }

        daoJ.delete(relatInDB);

        list = daoJ.findAll();

        for (Jepartnerem l : list) {
            if (l.getId_trh() == Integer.parseInt(s[1]) && l.getId_farmar() == Integer.parseInt(s[0])) {
                actual = l;
                break;
            }
        }

        //assert

        assertNotNull(relatInDB);
        assertNull(actual);
    }

    public boolean controlDataRelation(Farmar farmar, Trh trh, String[] s, LocalDate[] localDates){

        //jestli id trh nebo farmar je int
        if(!checkString(s[1]) || !checkString(s[0])){
            return false;
        }

        //jestli farmar aktualne pracuje s license
        if (!farmar.isActive()){
            return false;
        }

        //jestli existujou zaznamy farmar i trh
        if (trh == null){
            return false;
        }
        if (farmar == null){
            return false;
        }

        //jestli from neni vetsi nez to
        if(localDates[0].isAfter(localDates[1])){
            return false;
        }

        //jestli perioda neni vetsi 8 let a neni mensi 1 roku
        Period period = Period.between(localDates[0], localDates[1]);
        if(period.getYears() < 1 || period.getYears() > 8){
            return false;
        }

        //jestli se license konci drive, nez se konci contract
        Date date = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        if (farmar.getLicense().before(date)){
            return false;
        }

        //jestli date from je validni
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate startDate1 = LocalDate.parse("01.03.2017", formatter);
        LocalDate endDate1 = LocalDate.parse("01.05.2029", formatter);
        if (localDates[0].isBefore(startDate1) || localDates[0].isAfter(endDate1)){
            return false;
        }

        //jestli date to je validni
        LocalDate startDate2 = LocalDate.parse("01.03.2018", formatter);
        LocalDate endDate2 = LocalDate.parse("01.05.2029", formatter);
        if (localDates[1].isBefore(startDate2) || localDates[1].isAfter(endDate2)){
            return false;
        }

        //jestli trh a farmar ze stejnich mest
        if(!trh.getMesto().equals(farmar.getMesto())){
            return false;
        }

        return true;
    }

    public boolean checkString(String string) {
        try {
            Integer.parseInt(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}