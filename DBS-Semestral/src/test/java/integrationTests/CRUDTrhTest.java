package integrationTests;

import dao.DaoTrh;
import model.Trh;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CRUDTrhTest {

    private static Trh existTrh = null;

    @BeforeClass
    public static void before(){
        DaoTrh daoT = new DaoTrh();
        int id_trh = 70;

        if (daoT.findById(id_trh) != null){
            existTrh = daoT.findById(id_trh);
        }
    }

    @AfterClass
    public static void after(){
        DaoTrh daoT = new DaoTrh();

        if (existTrh != null){
            daoT.save(existTrh);

        }
    }

    @Test
    public void save_and_delete_Trh_pozitivni_pruhod(){

        // arrange

        DaoTrh daoT = new DaoTrh();

        int expectId = 70;
        String expectMesto = "Ust Nad Labem";
        String expectName = "TrhTestTest";
        int expectPsc = 34345;
        String expextTelefone = "420720293999";

        Trh trh = new Trh(expectId, expectMesto, expectName, expectPsc, expextTelefone);
        Trh actualTrh;
        Trh actualTrhIsRemove;

        //act

        daoT.save(trh);

        actualTrh = daoT.findById(expectId);

        daoT.delete(trh);

        actualTrhIsRemove = daoT.findById(expectId);

        int actualId = actualTrh.getId_trh();
        String actualMesto = actualTrh.getMesto();
        String actualName = actualTrh.getName();
        int actualPsc = actualTrh.getPsc();
        String actualTelefone = actualTrh.getTelefon();

        //assert

        assertNotNull(actualTrh);
        assertNull(actualTrhIsRemove);
        assertEquals(expectId, actualId);
        assertEquals(expectMesto, actualMesto);
        assertEquals(expectName, actualName);
        assertEquals(expectPsc, actualPsc);
        assertEquals(expextTelefone, actualTelefone);
    }
}

