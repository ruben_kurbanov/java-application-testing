package service;

import dao.DaoFarm;
import dao.DaoJepartnerem;
import model.Farmar;
import model.Jepartnerem;
import model.Trh;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;


public class JepartneremServiceTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private DaoJepartnerem daoJepa = Mockito.mock(DaoJepartnerem.class);
    private JepartneremService service;

    public JepartneremServiceTest(){
        MockitoAnnotations.initMocks(this);
        service = new JepartneremService(daoJepa);
    }

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void findAll_AllRecordsInDatabase_FoundAllRecords(){
        //arrange
        List<Jepartnerem> expectedList = createListRelation();

        //act

        Mockito.when(daoJepa.findAll()).thenReturn(expectedList);

        List<Jepartnerem> actualList = service.findAll();

        //assert

        assertEquals(expectedList, actualList);

    }

    @Test
    public void find_RecordExistInDatabase_ExpectedObject() throws Exception {

        //arrange
        List<Jepartnerem> list = createListRelation();

        Jepartnerem expected = list.get(0);

        String id_trh = "10";
        String id_farmar = "6";

        //act
        Mockito.when(daoJepa.findAll()).thenReturn(list);

        Jepartnerem actual = service.find(id_trh, id_farmar);

        //assert

        assertEquals(expected, actual);

    }

    @Test
    public void save_RecordIsAlreadyExistInDatabase_ExceptionThrown() throws Exception{

        //arrange
        Date license = Date.from(LocalDate.of(2031, 5, 24).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar farmar = new Farmar(6, "test@seznam.cz", "Brno", "Testttt", "420659265495", license, true);
        Trh trh = new Trh(10, "Brno", "TrhTest", 33333, "420720293410");

        String[] s = new String[]{
                "6",//farmar id
                "10"//trh id
        };

        LocalDate[] localDates = new LocalDate[]{
                LocalDate.of(2018,3,1), //contract from
                LocalDate.of(2023,3,1) //contract to
        };

        //act

        Mockito.when(daoJepa.findAll()).thenReturn(createListRelation());

        //assert

        assertThrows(Exception.class, () -> service.save(farmar,trh,s,localDates));
        assertEquals("Such an entry already exists in the database!", errContent.toString());

    }

    @Test
    public void update_DataIsOk_PassPositiveWithoutException() throws Exception {
        //arrange
        Date license = Date.from(LocalDate.of(2031, 5, 24).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Farmar farmar = new Farmar(6, "test@seznam.cz", "Brno", "Testttt", "420659265495", license, true);
        Trh trh = new Trh(10, "Brno", "TrhTest", 33333, "420720293410");

        String[] s = new String[]{
                "6",//farmar id
                "10"//trh id
        };

        LocalDate[] localDates = new LocalDate[]{
                LocalDate.of(2018,3,1), //contract from
                LocalDate.of(2023,3,1) //contract to
        };

        //assert & act

        assertDoesNotThrow(() -> service.update(farmar, trh, s, localDates, createRelationshipEntity()));
    }

    public List<Jepartnerem> createListRelation(){

        String[] s1 = new String[]{
                "6",//farmar id
                "10"//trh id
        };

        String[] s2 = new String[]{
                "7",//farmar id
                "10"//trh id
        };

        LocalDate[] localDates = new LocalDate[]{
                LocalDate.of(2018,3,1), //contract from
                LocalDate.of(2023,3,1) //contract to
        };
        Date date = Date.from(localDates[0].atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date date1 = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        List<Jepartnerem> list = new ArrayList<>();
        list.add(new Jepartnerem(Integer.parseInt(s1[1]), Integer.parseInt(s1[0]), date, date1));
        list.add(new Jepartnerem(Integer.parseInt(s2[1]), Integer.parseInt(s2[0]), date, date1));

        return list;

    }

    public Jepartnerem createRelationshipEntity(){
        String[] s1 = new String[]{
                "6",//farmar id
                "10"//trh id
        };

        LocalDate[] localDates = new LocalDate[]{
                LocalDate.of(2018,6,1), //contract from
                LocalDate.of(2023,3,1) //contract to
        };
        Date date = Date.from(localDates[0].atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date date1 = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        return new Jepartnerem(Integer.parseInt(s1[1]), Integer.parseInt(s1[0]), date, date1);
    }

}
