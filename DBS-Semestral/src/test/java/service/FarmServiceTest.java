package service;

import controller.MainController;
import dao.DaoFarm;
import dao.DaoJepartnerem;
import model.Farmar;
import model.Jepartnerem;
import model.Trh;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FarmServiceTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private DaoFarm daoFarm = Mockito.mock(DaoFarm.class);
    private DaoJepartnerem daoJepa = Mockito.mock(DaoJepartnerem.class);
    private FarmService service;

    public FarmServiceTest(){
        MockitoAnnotations.initMocks(this);
        service = new FarmService(daoFarm, daoJepa);
    }

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void checkString_CorrectStringToConvertToInteger_True(){

        // arrange
        String isInt = "123";

        //act
        boolean actualIsInt = service.checkString(isInt);

        //assert
        assertTrue(actualIsInt);
    }

    @Test
    public void isEmail_InvalidEmail_False(){

        // arrange
        String wrongEmail = "@kurba.ru@b*mmmm..cz";

        //act

        boolean actualWrongMail = service.isEmail(wrongEmail);

        //assert

        assertFalse(actualWrongMail);
    }

    @Test
    public void isWord_InvaliadString_False(){
        // arrange
        String incorrectWord = "word23W%rdwor8..";

        //act
        boolean actualIncorrectWord = service.isWord(incorrectWord);

        //assert
        assertFalse(actualIncorrectWord);
    }

    @Test
    public void findAll_AllRecordsInDatabase_FoundAllRecords(){
        // arrange
        List<Farmar> expectedList = new ArrayList<>();
        expectedList.add(createTestEntityFarmar());
        expectedList.add(createTestEntityFarmar2());

        //act
        Mockito.when(daoFarm.findAll()).thenReturn(expectedList);

        List<Farmar> actualList = daoFarm.findAll();

        //assert
        assertEquals(expectedList, actualList);

    }

    @Test
    public void find_RecordNoExistInDatabase_Null(){
        // arrange
        String id = "45";

        //act
        Mockito.when(daoFarm.findById(Integer.parseInt(id))).thenReturn(null);

        Farmar farmar = daoFarm.findById(Integer.parseInt(id));

        //assert
        assertNull(farmar);

    }

    @Test
    public void save_DataWithWrongEmail_ExceptionThrown() throws Exception {

        //arrange
        String[] invalidData = new String[]{
                "40",
                "kurbarub@ma@@./).cz",
                "Praha",
                "Farm",
                "420720293222"

        };
        boolean active = true;
        LocalDate license_to = LocalDate.of(2020,3,1);

        //act

        Mockito.when(daoFarm.findById(Integer.parseInt(invalidData[0]))).thenReturn(null);

        //assert
        assertThrows(Exception.class, () -> service.save(invalidData,license_to,active));
        assertEquals("Invalid email format! For example kurbarub@fel.cvut.cz", errContent.toString());

    }

    @Test
    public void delete_cannotBeDeletedIsInRelation_ExceptionThrown(){
        //arrange
        List<Jepartnerem> list = createListRelation();
        Farmar farmar= createTestEntityFarmar();

        //act
        Mockito.when(daoJepa.findAll()).thenReturn(list);

        //assert
        assertThrows(Exception.class, () -> service.delete(farmar));
        assertEquals("It is not possible to delete this object, because it is related to Trh in another table!", errContent.toString());

    }

    public List<Jepartnerem> createListRelation(){
        String[] s1 = new String[]{
            "6",//farmar id
            "10"//trh id
        };

        String[] s2 = new String[]{
                "7",//farmar id
                "10"//trh id
        };

        LocalDate[] localDates = new LocalDate[]{
            LocalDate.of(2018,3,1), //contract from
            LocalDate.of(2023,3,1) //contract to
        };
        Date date = Date.from(localDates[0].atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date date1 = Date.from(localDates[1].atStartOfDay(ZoneId.systemDefault()).toInstant());

        List<Jepartnerem> list = new ArrayList<>();
        list.add(new Jepartnerem(Integer.parseInt(s1[1]), Integer.parseInt(s1[0]), date, date1));
        list.add(new Jepartnerem(Integer.parseInt(s2[1]), Integer.parseInt(s2[0]), date, date1));

        return list;

    }

    public Farmar createTestEntityFarmar(){
        int id = 6;
        String name = "FirstFarm";
        String email = "firstfarm@seznam.cz";
        String city = "Praha";
        String phone = "420720293310";
        boolean isActive = true;
        LocalDate license = LocalDate.of(2023,3,1);
        Date date = Date.from(license.atStartOfDay(ZoneId.systemDefault()).toInstant());

        return new Farmar(id, email, city, name, phone, date, isActive);
    }

    public Farmar createTestEntityFarmar2(){
        int id = 7;
        String name = "SecondFarm";
        String email = "secondfarm@seznam.cz";
        String city = "Brno";
        String phone = "420720293888";
        boolean isActive = false;
        LocalDate license = LocalDate.of(2023,4,8);
        Date date = Date.from(license.atStartOfDay(ZoneId.systemDefault()).toInstant());

        return new Farmar(id, email, city, name, phone, date, isActive);
    }

}
