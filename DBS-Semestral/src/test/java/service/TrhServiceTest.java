package service;

import dao.DaoFarm;
import dao.DaoJepartnerem;
import dao.DaoTrh;
import model.Trh;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.persistence.Table;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class TrhServiceTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private DaoTrh daoTrh = Mockito.mock(DaoTrh.class);
    private DaoJepartnerem daoJepa = Mockito.mock(DaoJepartnerem.class);
    private TrhService service;

    public TrhServiceTest(){
        MockitoAnnotations.initMocks(this);
        service = new TrhService(daoTrh, daoJepa);
    }

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void find_NoRecordInDatabase_Null() throws Exception {
        //arrange
        int id_trh = 34;

        //act
        Mockito.when(daoTrh.findById(id_trh)).thenReturn(null);
        Trh actual = service.find("34");

        //assert
        assertNull(actual);

    }

    @Test
    public void findAll_AllRecordsInDatabase_ReturnsAllRecords(){

        //arrange
        List<Trh> expectedList = createListEntityTrh();

        //act

        Mockito.when(daoTrh.findAll()).thenReturn(expectedList);

        List<Trh> actualList = service.findAll();

        //assert
        assertEquals(expectedList, actualList);

    }

    @Test
    public void save_DataWhichAlreadyExistInDatabase_ExceptionThrown(){
        //arrange

        String[] s = {
                "10",
                "Liberec",
                "TrhCh",
                "65676",
                "420720293220"
        };

        //act
        Mockito.when(daoTrh.findById(10)).thenReturn(createEntityTrh());

        //assert
        assertThrows(Exception.class, ()->service.save(s));
        assertEquals("A record with the same ID already exists!", errContent.toString());
    }

    @Test
    public void update_WithCorrectData_PassPositiveWithoutException(){
        //arrange
        String[] s = {
                "10",
                "Liberec",
                "TrhCh",
                "65676",
                "420720293220"
        };

        //act

        Mockito.when(daoTrh.findById(10)).thenReturn(createEntityTrh());

        //assert
        assertDoesNotThrow(()->service.update(s));
    }

    public List<Trh> createListEntityTrh(){
        int id_trh = 10;
        String name = "Cerstvo";
        String mesto = "Praha";
        String telefon = "420720293310";
        int psc = 77777;
        List<Trh> list = new ArrayList<>();

        list.add(new Trh(10, "Brno", "TrhTest", 33333, "420720293410"));
        list.add(new Trh( id_trh, mesto, name, psc, telefon));


        return list;
    }

    public Trh createEntityTrh(){
        int id_trh = 10;
        String name = "Cerstvo";
        String mesto = "Praha";
        String telefon = "420720293310";
        int psc = 77777;

        return new Trh( id_trh, mesto, name, psc, telefon);
    }

}
